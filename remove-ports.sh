#! /bin/bash

declare -i count=0
declare -i count2=0
declare -a open_ports
declare -a allowed__ports
port_is_safe=0

#copies all open ports to a file
sudo firewall-cmd --list-ports > open_ports.txt

#puts all open ports from the file into an array
while read line; do
  for word in $line; do
    open_ports[count]=$(echo $word | cut -d/ -f1)
#    echo ${open_ports[count]}
    count=$((count+1))
  done
done < open_ports.txt

#puts all allowed ports from a self created file into an array
while read line2; do
  for word2 in $line2; do
    allowed_ports[count2]=$(echo $word2)
    #echo ${allowed_ports[count2]}
    count2=$((count2+1))
  done
done < allowed_ports.txt

#checks each of the open ports to see if it is listed in the allowed ports. If not, then print that port number
for port in "${open_ports[@]}"; do
  for port2 in "${allowed_ports[@]}"; do
    if [[ "$port" == "$port2" ]]; then
      port_is_safe=1
    fi
  done
  if [[ $port_is_safe -eq 0 ]]; then
    echo "$port"
  fi
  port_is_safe=0
done


